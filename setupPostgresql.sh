#hints how to make postgresql working


# need to add below to allow listening of postgresql
# listen_addresses = '*'
# vim /etc/postgresql/9.1/main/postgresql.conf

# and here we need to add sth like 
# host all all 0.0.0.0/0 md5
# to allow access from outside
# vim /etc/postgresql/9.1/main/pg_hba.conf



createUserAndDB(){
#sudotes the user  
#    with no database creation rights (-D) 
#    with no add user rights (-A) 
#    with no add role rights (-R)
#    and will prompt you for entering a password (-P).
# sudo -u postgres createuser -D -R -A -P youbook
   echo 'creating user:' $1 ' and db:' $2
    
    sudo -u postgres psql -c "DROP SCHEMA IF EXISTS $1;"
    sudo -u postgres psql -c "DROP DATABASE IF EXISTS $2;"
    sudo -u postgres psql -c "DROP ROLE IF EXISTS $1;"
    
    sudo -u postgres psql -c "CREATE ROLE $1 WITH LOGIN UNENCRYPTED PASSWORD '$1' NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT;"
    #create the database 'youbookdb with 'youbook' as owner.
    # sudo -u postgres createdb -O youbook youbookdb
    sudo -u postgres psql -c "CREATE DATABASE $2 OWNER $1;"
    sudo -u postgres psql -c "CREATE SCHEMA $1 AUTHORIZATION $1;"
}


createUserAndDB 'youbook_test' 'youbook_test_db'

#createUserAndDB 'teamcity' 'teamcitydb'
