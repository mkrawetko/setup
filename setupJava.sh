#!/bin/bash

# usually ubuntu server does not have add-apt-repository
# so lets install it
sudo apt-get install python-software-properties
# add repository
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
sudo apt-get install oracle-java8-set-default

java -version
