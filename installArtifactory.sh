#!/bin/bash
# installing artifactory as service on ubuntu server

askUser_JAVA_HOME(){
   JAVA_HOME_CANDIDATES=$(find /usr -type d -name 'java-*')
   echo "Found the following candidates for JAVA_HOME: "
   echo $JAVA_HOME_CANDIDATES
   echo "Which java shuld be used:"
   read USER_SUBMITTED_JAVA_HOME
   echo "You chose $USER_SUBMITTED_JAVA_HOME ."
   JAVA_HOME=${USER_SUBMITTED_JAVA_HOME}
}

APP_INSTALATION_DIR="/opt"

ARTIFACTORY_VERSION="3.2.0"
ARTIFACTORY_DIR_NAME="artifactory-$ARTIFACTORY_VERSION"
ARTIFACTORY_HOME="$APP_INSTALATION_DIR/$ARTIFACTORY_DIR_NAME"

[ -d $ARTIFACTORY_HOME ] && {
    echo -e "artifactory seems to be already installed in: $ARTIFACTORY_HOME \nSkipping installation !!!"; exit 0; }

cd $APP_INSTALATION_DIR ; echo -n "instalation dir: "; pwd;

wget http://sourceforge.net/projects/artifactory/files/artifactory/$ARTIFACTORY_VERSION/$ARTIFACTORY_DIR_NAME.zip
unzip $ARTIFACTORY_DIR_NAME.zip 
rm -f $ARTIFACTORY_DIR_NAME.zip

cd $ARTIFACTORY_DIR_NAME/bin; echo -n pwd: ; pwd;
chmod +x installService.sh && ./installService.sh

askUser_JAVA_HOME
echo "export JAVA_HOME=$JAVA_HOME" >> /etc/opt/jfrog/artifactory/default

service artifactory check

